function init() {
  function menu() {
    const menu = document.querySelector(".menu");
    const popDown = document.querySelector(".pop-down");

    let showMenu = false;
    menu.addEventListener("click", toggleMenu);
    console.log("clicked");
    function toggleMenu() {
      if (!showMenu) {
        menu.classList.add("open");
        popDown.style.transform = "translateY(0)";
        popDown.style.display = "block";
        showMenu = true;
      } else {
        menu.classList.remove("open");
        popDown.style.transform = "translateY(-220%)";

        showMenu = false;
      }
    }
  }

  menu();
}

init();
