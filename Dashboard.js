function init() {
  // sidebar toggle functionality with menu icon 
  function asideMenu() { 
    const sideMenu = document.querySelector("aside");
    const menuBtn = document.querySelector("#menu-btn");
    const closeBtn = document.querySelector("#close-btn");

    if (menuBtn == null) return;
    menuBtn.addEventListener("click", () => {
      console.log("cliked");
      sideMenu.style.display = "block";
    });

    if (closeBtn == null) return;
    closeBtn.addEventListener("click", () => {
      sideMenu.style.display = "none";
    });
  }

  asideMenu();
  const sideMenu = document.querySelector("aside");
  const container = document.querySelector(".container");

  container.addEventListener("resize", () => {
    console.log("resizing");
    if (document.body.clientWidth >= 768) {
      console.log(document.body.clientWidth);
      sideMenu.style.display = "block";
    }
  });

  // show full user details onclick 
  function userDetails() {
    const usersDiv = document.querySelectorAll(".userWrapper table tbody img");
    const overLay = document.getElementById("overlay");
    const modalClose = document.querySelectorAll(".modal-close");
    const friendsDiv = document.querySelectorAll(".friends-div");
    const userDetails = document.querySelector(".user-details");
    const productBtn = document.querySelectorAll(".btnsFlex");
    const productAddBtns = document.querySelectorAll(".pdt-img1 svg");
    const productAddDiv = document.getElementById("product-add");
    const productAddImg = document.getElementById("product-added-img");
    const uploadProductBtns = document.querySelectorAll(".product-btn");
    const orderModalBtns = document.getElementById("OrderModal");
    const modalOrderClose = document.querySelectorAll(".modal-order-close");

    usersDiv.forEach((user) => {
      user.addEventListener("click", () => {
        console.log("clicked");
        const modal = document.querySelector(user.dataset.modalTarget);
        console.log(modal);
        openModal(modal);
      });
    });
    friendsDiv.forEach((friend) => {
      friend.addEventListener("click", () => {
        console.log("clicked");
        const modal = document.querySelector(friend.dataset.modalTarget);
        console.log(modal);
        openModal(modal);
      });
    });

    if(overLay == null) return;
    overLay.addEventListener("click", () => {
      console.log("clicked");
      const modals = document.querySelectorAll(".user-details.popUp");
      modals.forEach((modal) => {
        closeModal(modal);
      });
    });
    modalClose.forEach((button) => {
      button.addEventListener("click", () => {
        const modal = button.closest("#modal");
        console.log(modal);
        closeModal(modal);
      });
    });
    // Order Btn
    if (orderModalBtns == null) return;
    orderModalBtn.addEventListener("click", () => {
      console.log("click");
      const modal = document.querySelector(orderModalBtn.dataset.modalTarget);
      console.log(modal);
      openOrderModal(modal);
    });

    modalOrderClose.forEach((button) => {
      button.addEventListener("click", () => {
        const modal = button.closest("#OrderModal");
        console.log(modal);
        closeOrderModal(modal);
      });
    });

    overLay.addEventListener("click", () => {
      console.log("clicked");
      const modals = document.querySelectorAll(".user-details.popUpGrid");
      modals.forEach((modal) => {
        closeOrderModal(modal);
      });
    });

    function openOrderModal(order) {
      if (order == null) return;
      order.classList.add("popUpGrid");
      overLay.classList.add("active");
    }
    function closeOrderModal(order) {
      if (order == null) return;
      order.classList.remove("popUpGrid");
      overLay.classList.remove("active");
    }
    // End of Order Modal

    //  Upload Product
    if (uploadProductBtns == null) return;
    uploadProductBtns.forEach((uploadProductBtn) => {
      uploadProductBtn.addEventListener("click", () => {
        const modal = document.querySelector(
          uploadProductBtn.dataset.modalTarget
        );
        console.log(modal);

        const firstModal = document.querySelector(".user-details.popUp");
        closeModal(firstModal);
        openSuccessModal(modal);
      });

      function openSuccessModal(detail) {
        if (detail == null) return;
        detail.classList.add("popUpFlex");
        overLay.classList.add("active");
      }
    });
    if (productAddBtns == null) return;
    productAddBtns.forEach((productAddBtn) => {
      productAddBtn.addEventListener("click", () => {
        console.log("clicked");
        productAddDiv.classList.add("none");
        productAddImg.classList.remove("none");
      });
    });

    productBtn.forEach((friend) => {
      friend.addEventListener("click", () => {
        console.log("clicked");
        const modal = document.querySelector(friend.dataset.modalTarget);
        console.log(modal);
        openModal(modal);
      });
    });

    if (userDetails == null) return;
    userDetails.addEventListener("click", () => {
      const modals = document.querySelectorAll(".Friends-pop.popUp");
      modals.forEach((modal) => {});
    });

    function closeFriendModal(friendContact) {
      if (friendContact == null) return;
      if (!friendContact.closest(".friends-pop"))
        friendContact.classList.remove("popUp");
    }

    overLay.addEventListener("click", () => {
      const modals = document.querySelectorAll(".user-details.popUp");
      modals.forEach((modal) => {
        closeModal(modal);
      });
    });

    function closeModal(detail) {
      console.log("clicked");
      if (detail == null) return;
      detail.classList.remove("popUp");
      overLay.classList.remove("active");
    }

    function openModal(detail) {
      if (detail == null) return;
      detail.classList.add("popUp");
      overLay.classList.add("active");
    }
  }

  userDetails();

  function publish() {
    const publishBtns = document.querySelectorAll(".btns");
    const overLay = document.getElementById("overlay");

    if (publishBtns == null) return;
    publishBtns.forEach((publishBtn) => {
      publishBtn.addEventListener("click", () => {
        console.log("clicked");
        const modal = document.querySelector(publishBtn.dataset.modalTarget);
        openSuccessModal(modal);
      });
    });

    if (overLay == null) return;
    overLay.addEventListener("click", () => {
      const modals = document.querySelectorAll(".success-pop.popUpFlex");
      modals.forEach((modal) => {
        closeModal(modal);
      });
    });
    function openSuccessModal(detail) {
      if (detail == null) return;
      detail.classList.add("popUpFlex");
      overLay.classList.add("active");
    }

    function closeModal(detail) {
      console.log("clicked");
      if (detail == null) return;
      detail.classList.remove("popUpFlex");
      overLay.classList.remove("active");
    }
  }
  publish();

  function configuration() {
    const openConfigs = document.querySelectorAll("[data-modal-target]");
    const eventDivs = document.querySelectorAll(".divs p");
    const editBtn = document.querySelector(".Edit-profile-pic");
    const profileHolder = document.querySelector(".profileHold");
    const updateBtn = document.querySelector(".update-profile-pic");

    if (editBtn == null) return;
    editBtn.addEventListener("click", () => {
      const editItemsModals = document.querySelectorAll(
        editBtn.dataset.modalTarget
      );
      openEditModal(editItemsModals);
    });

    function openEditModal(editmodals) {
      if (editmodals == null) return;
      editmodals.forEach((editItemsModal) => {
        editItemsModal.classList.remove("none");
        editItemsModal.classList.add("popUpTable");
        profileHolder.classList.add("none");
      });
    }

    // Tracker
    let current = 0;
    eventDivs.forEach((eventDivs, index) => {
      eventDivs.addEventListener("click", (e) => {
        changeActive(e.target);
        changeItems(index);
      });
    });

    function changeActive(item) {
      if (item == null) return;
      // const modals = modal.querySelectorAll("[data-modal-target]");
      eventDivs.forEach((div) => {
        div.classList.remove("success2");
      });
      item.classList.add("success2");
    }

    function changeItems(itemNumber) {
      const nextItem = openConfigs[itemNumber];
      const currentItem = openConfigs[current];

      openConfigs.forEach((openConfig) => {
        const allModals = document.querySelector(
          openConfig.dataset.modalTarget
        );
        allModals.classList.add("none");

        nextItem.addEventListener("click", () => {
          const Nextmodals = document.querySelectorAll(
            nextItem.dataset.modalTarget
          );
          const currentModals = document.querySelectorAll(
            currentItem.dataset.modalTarget
          );
          currentModals.forEach((currentModal) => {
            currentModal.classList.add("none");
          });
          Nextmodals.forEach((Nextmodal) => {
            Nextmodal.classList.remove("none");
            Nextmodal.classList.add("popUpTable");
          });
        });
      });

      current = itemNumber;
    }
  }
  configuration();
}

init();
